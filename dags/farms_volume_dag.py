from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.python import PythonOperator
import pandas as pd
from google.cloud import storage
from google.cloud import bigquery
import os

# Function to load and aggregate shipment data from GCS
def load_and_aggregate_shipments_data(bucket_name, source_blob_name):
    """Loads and aggregates shipment data by country from a CSV file in GCS."""
    storage_client = storage.Client()
    bucket = storage_client.bucket(bucket_name)
    blob = bucket.blob(source_blob_name)
    temp_file = '/tmp/soy_shipments_data.csv'
    blob.download_to_filename(temp_file)
    
    df = pd.read_csv(temp_file)
    aggregated_data = df.groupby('Country')['Volume'].sum().reset_index()
    os.remove(temp_file)
    return aggregated_data

# Function to insert data into BigQuery
def insert_into_bigquery(aggregated_data, project_dataset_name, table_name):
    """Inserts aggregated data into a BigQuery table."""
    client = bigquery.Client()
    table_id = f"{project_dataset_name}.{table_name}"
    
    job_config = bigquery.LoadJobConfig(
        schema=[
            bigquery.SchemaField("Country", "STRING"),
            bigquery.SchemaField("Volume", "FLOAT"),
        ],
        write_disposition=bigquery.WriteDisposition.WRITE_TRUNCATE,
    )
    
    job = client.load_table_from_dataframe(aggregated_data, table_id, job_config=job_config)
    job.result()  # Wait for the job to complete

    print(f"Inserted {job.output_rows} rows into {table_id}.")

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2023, 1, 1),
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}

dag = DAG(
    'read_and_insert_shipmentdata',
    default_args=default_args,
    description='Retrieve shipment data from GCS and insert into BigQuery',
    schedule_interval=None,
    catchup=False,
)

def task_wrapper(**kwargs):
    aggregated_data = load_and_aggregate_shipments_data(bucket_name='tract-data-engineering-test-task', source_blob_name='soy_shipments_data.csv')
 
    insert_into_bigquery(aggregated_data=aggregated_data, project_dataset_name='data-engineering-test-task.deforestation', table_name='shipments_report')

# Define a single PythonOperator task to perform both load and insert operations
load_and_insert_task = PythonOperator(
    task_id='load_and_insert_shipments_data',
    python_callable=task_wrapper,
    dag=dag,
)

